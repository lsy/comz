<?php

namespace Admin\Controller;

class ModConfigController extends ModController
{
    static $export_menu = array(
        'system' => array(
            '网站设置' => array(
                'basic' => array(
                    'title' => '基本设置',
                    'hiddens' => array()
                ),
                'seo' => array(
                    'title' => 'SEO设置',
                    'hiddens' => array()
                ),
                'contact' => array(
                    'title' => '联系方式',
                    'hiddens' => array()
                ),
                'domain' => array(
                    'title' => '域名绑定',
                    'hiddens' => array()
                )
            )
        )
    );

    public function domain()
    {
        if (IS_POST) {

            $apps = I('post.app', array());
            $domains = I('post.domain', array());

            $mapping = array();
            for ($i = 0; $i < count($apps); $i++) {
                $domain = isset($domains[$i]) ? $domains[$i] : '';
                $app = $apps[$i];
                if (!empty($domain) && !empty($app)) {
                    $mapping[$domain] = $app;
                }
            }

            tpx_sys_config_set('APP_SUB_DOMAIN_RULES', $mapping);

            $this->success('保存成功');
        }

        $domains = C('APP_SUB_DOMAIN_RULES');
        if (empty($domains)) {
            $domains = array();
        }

        $apps = array('Home' => '主程序');

        $this->data_apps = $apps;
        $this->data_domains = $domains;

        $this->display();
    }

    public function basic()
    {
        $keys = array(
			'home_name',
            'basic_copyright',
            'domain_no',
			'theme_color',
			'image_logo',
			'code_counter_position',
            'code_counter',
        );
        if (IS_POST) {
            foreach ($keys as &$k) {
                tpx_config($k, I('post.' . $k, '', 'trim'));
            }
			foreach (array('image_logo') as $img) {
                $old = tpx_config_get($img);
                $new = upload_tempfile_save_storage('image', $old);
                if ($new) {
                    tpx_config($img, 'data/image/' . $new);
                }
            }
            if (I('post.system_rewrite_enable', 0, 'intval')) {
                tpx_sys_config_set('URL_MODEL', 2);
            } else {
                tpx_sys_config_set('URL_MODEL', 3);
            }

            $this->success('保存成功');
        }

        foreach ($keys as &$k) {
            $this->$k = tpx_config_get($k, '');
        }

        $this->system_rewrite_enable = (tpx_sys_config_get('URL_MODEL') == 2);
        $this->image_logo = tpx_config_get('image_logo', 'asserts/res/image/logo.png');	
        $this->display('ModConfig:basic');
    }

    public function seo()
    {
        $keys = array(
            'home_title',
            'home_keywords',
            'home_description',
			'home_about',
        );
        if (IS_POST) {
            foreach ($keys as &$k) {
                tpx_config($k, I('post.' . $k, '', 'trim'));
            }
            $this->success('保存成功');
        }//IS_POST

        foreach ($keys as &$k) {
            $this->$k = tpx_config_get($k, '');
        }

        $this->display('ModConfig:seo');
    }	

    public function contact()
    {
        $keys = array(
            'contact_address',
            'contact_email',
            'contact_website',
            'contact_tel',
            'contact_qq',
			'image_qr',
        );
        if (IS_POST) {
            $data = array();
            foreach ($keys as &$k) {
                tpx_config($k, I('post.' . $k, '', 'trim'));
            }
			
			foreach (array('image_qr') as $img) {
                $old = tpx_config_get($img);
                $new = upload_tempfile_save_storage('image', $old);
                if ($new) {
                    tpx_config($img, 'data/image/' . $new);
                }
            }
            $this->success('保存成功');
        }

        foreach ($keys as &$k) {
            $this->$k = tpx_config($k);
        }
        $this->image_qr = tpx_config_get('image_qr', 'asserts/res/image/qr.jpg');
        $this->display('ModConfig:contact');
    }

}