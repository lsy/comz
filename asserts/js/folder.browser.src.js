(function ($) {
    $.fn.FolderBrowser = function (opt) {

        var
            me = this,
            opt = $.extend({
                url: '',
                callback: function (url, thumb) {
                    alert('url:' + url + ', thumb:' + thumb);
                },
                image: '',
                folders: 'image'
            }, opt);

        return this.each(function () {

            var $me = $(this),
                opts = opt,
                id = $me.attr('id'),
                previewDialog;

            var load = function (page) {
                $.get(opts.url,
                    {folders: opts.folders, page: page, pageSize: 10},
                    function (data) {
                        if (data.state == 'SUCCESS') {
                            var htmls = [];
                            for (var i = 0; i < data.list.length; i++) {
                                htmls.push(
                                    '<div class="item" data-url="' +
                                    data.list[i].url + '" data-thumb="' + data.list[i].thumb + '"' +
                                    'style="cursor:pointer;width:150px;height:150px;padding:5px;position:relative;float:left;">' +
                                    '<div style="border:1px solid #CCC;" onmouseover="this.style.borderColor=\'#F47229\';" onmouseout="this.style.borderColor=\'#CCC\';"><div><img src="' +
                                    data.list[i].thumb + '" style="width:138px;height:138px;" /></div>' +
                                    '<div class="text" style="background:rgba(0,0,0,0.5);color:#FFF;position:absolute;bottom:6px;left:6px;overflow:hidden;line-height:30px;width:138px;">'
                                    + data.list[i].filename + '</div></div></div>'
                                );
                            }
                            var $list = $(previewDialog.DOM.content).find('.folder_file_list');
                            $list.append(htmls.join(''));
                            $list.animate({scrollTop: (page * 150 * 3) + 'px'});
                            if (data.hasMore) {
                                $(previewDialog.DOM.content).find('.folder_file_more').show();
                            }
                        } else {
                            $.dialog.alert(data.state);
                        }
                    });
            };

            $me.css({display: 'inline-block', verticalAlign: 'bottom', padding: '0 5px'})
                .attr('title', '浏览').attr('data-toggle', 'tooltip');
            $me.html(['<a href="#" class="folder_browser_btn"><img src="', opt.image, '" /></a>'].join(''));
            $me.find('.folder_browser_btn').on('click', function (e) {
                e && e.preventDefault();
                var page = 1;
                previewDialog = $.dialog({
                    lock: true,
                    title: '选择',
                    max: false,
                    min: false,
                    fixed: true,
                    //icon: 'success.gif',
                    content: '<div class="folder_file_list" style="width:600px;height:' + ($(window).height() - 200) + 'px;overflow:auto;"></div>' +
                    '<div class="folder_file_more" style="display:none;text-align:center;font-size:12px;color:#999;line-height:30px;padding-top:10px;border-top:1px solid #CCC;cursor:pointer;">点击加载更多</div>',
                    init: function () {
                        load(page);
                        var $list = $(this.DOM.content).find('.folder_file_list');
                        $list.on('click', '.item', function () {
                            var url = $(this).attr('data-url');
                            var thumb = $(this).attr('data-thumb');
                            opts.callback(url, thumb);
                            previewDialog.close();
                        });
                        $(this.DOM.content).find('.folder_file_more').on('click', function () {
                            load(page);
                        });
                    }
                });
                return false;
            });

        });


    };

}(jQuery));
